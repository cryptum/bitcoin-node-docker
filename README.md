# docker-bitcoin-node

Set up variables in `.env`

Run command:

```bash
ulimit -n 1048576
docker run --ulimit nofile=1048576:1048576 --rm lukechilds/electrumx:v1.15.0 sh -c "ulimit -n" 1048576
docker-compose up -d
```

## Issues

### Error on ElectrumX: "struct.error: 'H' format requires 0 <= number <= 65535"

Make sure electrumx container is stopped and run the following command:

```bash
docker-compose run electrumx electrumx_compact_history
```

https://bitcoin.org/en/developer-reference

https://github.com/ruimarinho/docker-bitcoin-core
